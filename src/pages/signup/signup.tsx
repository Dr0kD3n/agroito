import { useContext } from "react";
import { useHistory } from "react-router-dom";
import { Form, Input, Button, Row, Col, message, Radio } from "antd";

import { rolesMap } from "store/userStore";

import { Context } from "../..";

const options = Object.entries(rolesMap).map(([key, value]) => ({
  value: key,
  label: value,
}));

export function Signup() {
  const history = useHistory();
  const { auth, firestore } = useContext(Context);

  const handleFinish = (data: any) => {
    const { email, password, role } = data;
    auth
      .createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        firestore.collection("usersCollection").add({
          uid: userCredential.user!.uid,
          role,
        });
        history.push("/login");
      })
      .catch((err) => message.error(err.message));
  };

  const handleError = (error: any) => {
    message.error(error.message);
  };

  return (
    <Form
      layout="vertical"
      onFinish={handleFinish}
      onFinishFailed={handleError}
    >
      <Row gutter={[0, 12]}>
        <Col span={24}>
          <Form.Item name="email" label="Email">
            <Input size="large" />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item name="password" label="Пароль">
            <Input size="large" type="password" />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item name="role" initialValue={options[0].value}>
            <Radio.Group optionType="button" options={options} size="large" />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Button size="large" htmlType="submit" type="primary">
            Создать аккаунт
          </Button>
        </Col>
      </Row>
    </Form>
  );
}
