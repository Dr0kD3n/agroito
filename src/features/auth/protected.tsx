import React, { useContext } from "react";
import { Route, RouteProps, Redirect } from "react-router-dom";

import { AuthContext } from "./auth";

export interface PrivateRouteProps extends Omit<RouteProps, "component"> {
  component: React.ReactNode;
}

export function PrivateRoute({
  component: RouteComponent,
  ...props
}: PrivateRouteProps) {
  const { currentUser } = useContext(AuthContext);

  const isLogged = !!currentUser;

  if (!isLogged) return <Redirect to="/login" />;

  return <Route {...props}>{RouteComponent}</Route>;
}
