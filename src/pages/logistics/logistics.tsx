import { useContext } from "react";
import { Link } from "react-router-dom";
import { useCollectionData } from "react-firebase-hooks/firestore";
import { useStore, useEvent } from "effector-react";
import { Col, List, Avatar, Typography, Button, Row } from "antd";

import { $logist, logistDeselected, logistSelected } from "store/cartStore";

import { Context } from "../..";

const { Text } = Typography;

export function Logistics() {
  const { firestore } = useContext(Context);
  const logist = useStore($logist);
  const selectLogist = useEvent(logistSelected);
  const deselectLogist = useEvent(logistDeselected);

  const [companies, isLoading] = useCollectionData(
    firestore.collection("logistics"),
    { idField: "id" }
  );

  if (isLoading) return <div>loading</div>;

  const renderCompanies = companies!.map(
    ({ name, logo, description, priceForKm, deliveryTime, id }) => {
      const price = priceForKm * 20;
      const isSelected = logist?.id === id;

      const select = () => selectLogist({ id });

      const renderButton = () => {
        if (!isSelected)
          return (
            <Button size="large" type="primary" onClick={select}>
              Выбрать
            </Button>
          );
      };

      return (
        <List.Item
          key={id}
          extra={[renderButton()]}
          style={{ backgroundColor: isSelected ? "#fafafa" : "white" }}
        >
          <List.Item.Meta
            avatar={<Avatar src={logo} />}
            title={<a href="https://ant.design">{name}</a>}
            description={
              <>
                <Col>
                  <Text>{description}</Text>
                </Col>
                <Col>
                  <Text>Цена: {price} рублей</Text>
                </Col>

                <Col>
                  <Text>Время доставки: {deliveryTime}</Text>
                </Col>
              </>
            }
          />
        </List.Item>
      );
    }
  );

  return (
    <Row gutter={[0, 48]}>
      <Col span={24}>
        <List>{renderCompanies}</List>
      </Col>
      <Col span={24}>
        <Row justify="end" gutter={12}>
          <Col>
            <Link to="/bank">
              <Button size="large" type="ghost" onClick={() => deselectLogist()}>
                Пропустить
              </Button>
            </Link>
          </Col>
          <Col>
            <Link to="/bank">
              <Button size="large" type="primary" disabled={!logist}>
                Продолжить
              </Button>
            </Link>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
