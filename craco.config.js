const path = require("path");
module.exports = {
  webpack: {
    alias: {
      '@components': path.resolve(__dirname, "src/components/"),
      '@features': path.resolve(__dirname, "src/assets/features/"),
      '@pages': path.resolve(__dirname, "src/assets/pages/"),
      '@store': path.resolve(__dirname, "src/assets/store/"),
    }
  }
}