import firebase from "firebase";
import { createStore, createEvent } from "effector";

interface User extends firebase.User {
  role: string;
}

type Roles = { [key: string]: string };

export const rolesMap: Roles = {
  customer: "Покупатель",
  vendor: "Продавец",
  agent: "Агент",
};

export const userSet = createEvent<User>();
export const userUnset = createEvent();

export const $user = createStore<User | null>(null);
export const $authed = $user.map((data) => !!data);

export const role = $user.map((user) =>
  user
    ? {
        name: user.role,
        label: rolesMap[user.role],
      }
    : null
);

$user.on(userSet, (_, user) => user).reset(userUnset);
