import { useContext } from "react";
import { useEvent } from "effector-react";
import { useHistory } from "react-router-dom";
import { Form, Input, Button, Row, Col, message } from "antd";

import { userSet } from "store/userStore";

import { Context } from "../..";

export function Login() {
  const history = useHistory();
  const setUser = useEvent(userSet);
  const { firebase, firestore, auth } = useContext(Context);

  const handleFinish = (data: any) => {
    const { email, password } = data;

    auth
      .setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      .then(() => {
        auth
          .signInWithEmailAndPassword(email, password)
          .then((userCredential) => {
            firestore
              .collection("usersCollection")
              .where("uid", "==", userCredential.user!.uid)
              .get()
              .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                  const { role } = doc.data();

                  setUser({
                    ...userCredential.user!,
                    role,
                  });
                });
              });
            history.push("/");
          })
          .catch((err) => message.error(err.message));
      })
      .catch((error) => {
        console.log({ error });
      });
  };

  const handleError = (error: any) => {
    message.error(error.message);
  };

  return (
    <Form
      layout="vertical"
      onFinish={handleFinish}
      onFinishFailed={handleError}
    >
      <Row gutter={[0, 12]}>
        <Col span={24}>
          <Form.Item name="email" label="Email">
            <Input size="large" />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Form.Item name="password" label="Пароль">
            <Input size="large" type="password" />
          </Form.Item>
        </Col>
        <Col span={24}>
          <Button size="large" htmlType="submit" type="primary">
            Войти
          </Button>
        </Col>
      </Row>
    </Form>
  );
}
