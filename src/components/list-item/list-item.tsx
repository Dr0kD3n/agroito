import { Link } from "react-router-dom";
import { List, Col, Row, Image, Typography } from "antd";

const { Title, Text } = Typography;

export interface ListItemProps {
  id: string;
  title: string;
  subtitle: string;
  location: string;
  author: string;
  description: string;
  image: string;
}

export function ListItem({
  id,
  title,
  subtitle,
  location,
  image,
  author,
}: ListItemProps) {
  return (
    <Link to={`lot/${id}`}>
      <List.Item>
        <Row gutter={24} align="middle">
          <Col>
            <Image
              preview={false}
              width={200}
              height={200}
              style={{ objectFit: "cover" }}
              alt="logo"
              src={image}
            />
          </Col>
          <Col flex="1">
            <Row justify="space-between">
              <Col span={24}>
                <Title level={3}>{title}</Title>
              </Col>
              <Col span={24}>
                <Text>{subtitle}</Text>
              </Col>
              <Col span={24}>
                <Text>{location}</Text>
              </Col>
              <Col span={24}>
                <Text>{author}</Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </List.Item>
    </Link>
  );
}
