import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Layout, Menu, Row, Space } from "antd";
import { LogoutOutlined, LoginOutlined } from "@ant-design/icons";
import "antd/dist/antd.css";
import { useStore } from "effector-react";

import { PrivateRoute } from "features/auth";
import { role, $authed } from "store/userStore";

import { Home } from "pages/home";
import { SearchResult } from "pages/search-result";
import { Lot } from "pages/lot";
import { Login } from "pages/login";
import { Logout } from "pages/logout";
import { Signup } from "pages/signup";
import { Logistics } from "pages/logistics";
import { Bank } from "pages/bank";
import { Payout } from "pages/payout";

import styles from "./App.module.css";

const { Header, Content } = Layout;

function App() {
  const isUserAuthed = useStore($authed);
  const userRole = useStore(role);

  return (
    <Router>
      <Layout style={{ height: "100vh", overflow: "auto" }}>
        <Header>
          <Row justify="end" align="bottom">
            <Menu theme="dark" mode="horizontal" disabledOverflow>
              {isUserAuthed && (
                <Menu.Item key="role">Роль: {userRole?.label}</Menu.Item>
              )}
              <Menu.Item key="authed">
                {isUserAuthed ? (
                  <Link to="/logout">
                    <Space size={12}>
                      <LogoutOutlined /> Выйти
                    </Space>
                  </Link>
                ) : (
                  <Link to="/login">
                    <Space size={12}>
                      <LoginOutlined /> Войти
                    </Space>
                  </Link>
                )}
              </Menu.Item>
            </Menu>
          </Row>
        </Header>
        <Content style={{ padding: "50px 25px" }}>
          <div className={styles.container}>
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route exact path="/search-result">
                <SearchResult />
              </Route>
              <Route exact path="/lot/:itemId">
                <Lot />
              </Route>
              <Route exact path="/login">
                <Login />
              </Route>
              <Route exact path="/logout">
                <Logout />
              </Route>
              <Route exact path="/signup">
                <Signup />
              </Route>
              <Route exact path="/logistics">
                <Logistics />
              </Route>
              <Route exact path="/bank">
                <Bank />
              </Route>
              <Route exact path="/payout">
                <Payout />
              </Route>
              <PrivateRoute path="/secret" component={<div>123</div>} />
              <Route path="">404</Route>
            </Switch>
          </div>
        </Content>
      </Layout>
    </Router>
  );
}

export default App;
