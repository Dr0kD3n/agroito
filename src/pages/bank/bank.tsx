import { useContext } from "react";
import { Link } from "react-router-dom";
import { useCollectionData } from "react-firebase-hooks/firestore";
import { Col, List, Avatar, Typography, Button, Row } from "antd";
import { useStore, useEvent } from "effector-react";

import { $bank, bankSelected, bankDeselected } from "store/cartStore";

import { Context } from "../..";

const { Text } = Typography;

export function Bank() {
  const { firestore } = useContext(Context);
  const bank = useStore($bank);
  const selectBank = useEvent(bankSelected);
  const deselectBank = useEvent(bankDeselected);

  const [banks, isLoading] = useCollectionData(firestore.collection("banks"), {
    idField: "id",
  });

  if (isLoading) return <div>loading</div>;

  const renderBanks = banks!.map(
    ({ name, logo, maxLoan, period, percent, id }) => {
      const isSelected = bank?.id === id;

      const select = () => selectBank({ id });

      const renderButton = () => {
        if (!isSelected)
          return (
            <Button size="large" type="primary" onClick={select}>
              Выбрать
            </Button>
          );
      };

      const normalizePrice = (price: number) => {
        const p = price.toString();
        const length = p.length;

        let res: string = `${price} руб.`;

        switch (true) {
          case length >= 7:
            res = `${p.substring(0, length - 6)} млн.`;
            break;
          case length >= 5:
            res = `${p.substring(0, length - 3)} тыс.`;
            break;
        }

        return res;
      };

      const normalizeYears = (years: number) => {
        const y = years.toString();
        const lastDigit = Number(y.substr(y.length - 1));

        switch (true) {
          case lastDigit === 1:
            return `${y} год`;
          case lastDigit > 1 && lastDigit < 5:
            return `${y} года`;
          default:
            return `${y} лет`;
        }
      };

      return (
        <List.Item
          key={id}
          extra={[renderButton()]}
          style={{ backgroundColor: isSelected ? "#fafafa" : "white" }}
        >
          <List.Item.Meta
            avatar={<Avatar src={logo} />}
            title={<a href="https://ant.design">{name}</a>}
            description={
              <>
                <Col>
                  <Text>До {normalizePrice(maxLoan)} рублей</Text>
                </Col>
                <Col>
                  <Text>На {normalizeYears(period)}</Text>
                </Col>

                <Col>
                  <Text>Под {percent}%</Text>
                </Col>
              </>
            }
          />
        </List.Item>
      );
    }
  );

  return (
    <Row gutter={[0, 48]}>
      <Col span={24}>
        <List>{renderBanks}</List>
      </Col>
      <Col span={24}>
        <Row justify="end" gutter={12}>
          <Col>
            <Link to="/payout">
              <Button size="large" type="ghost" onClick={deselectBank}>
                Пропустить
              </Button>
            </Link>
          </Col>
          <Col>
            <Link to="/payout">
              <Button size="large" type="primary" disabled={!bank}>
                Продолжить
              </Button>
            </Link>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
