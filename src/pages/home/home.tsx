import { Link } from "react-router-dom";
import { Form, Input, Button, Row, Col } from "antd";

export function Home() {
  const handleFinish = (data: any) => {
    console.log({ data });
  };

  return (
    <Form onFinish={handleFinish}>
      <Row gutter={24}>
        <Col span={20}>
          <Form.Item name="searchInput">
            <Input placeholder="Поиск" size="large" />
          </Form.Item>
        </Col>
        <Col flex="1">
          <Form.Item name="searchButton">
            <Link to="/search-result">
              <Button htmlType="submit" size="large" block>
                Найти
              </Button>
            </Link>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
}
