import { Button, message } from "antd";
import { useEvent, useStore } from "effector-react";
import { useEffect } from "react";

import { orderCreated, $orderStatus } from "store/cartStore";

export function Payout() {
  const { loading, error, done } = useStore($orderStatus);
  const createOrder = useEvent(orderCreated);

  useEffect(() => {
    if (!done) return;

    if (!error) return message.success("Заказ успешно сформирован");

    message.error("Что-то пошло не так");
    console.log({ error });
  }, [error, done]);

  return (
    <Button
      size="large"
      type="primary"
      onClick={() => createOrder()}
      loading={loading}
    >
      Заплатить многа деняг
    </Button>
  );
}
