import React from "react";
import ReactDOM from "react-dom";
import firebase from "firebase";
import "firebase/firestore";
import "firebase/auth";

import { AuthProvider } from "./features/auth";

import App from "./App";
import reportWebVitals from "./reportWebVitals";

const auth = firebase.auth();
const firestore = firebase.firestore();

export const Context = React.createContext({
  firebase,
  firestore,
  auth,
});

ReactDOM.render(
  <AuthProvider>
    <Context.Provider
      value={{
        firebase,
        firestore,
        auth,
      }}
    >
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </Context.Provider>
  </AuthProvider>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
