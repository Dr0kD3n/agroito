import { useContext } from "react";
import { List } from "antd";
import { useCollectionData } from "react-firebase-hooks/firestore";

import { Context } from "../..";
import { ListItem, ListItemProps } from "../../components/list-item";

export function SearchResult() {
  const { firestore } = useContext(Context);

  const [entries, loading] = useCollectionData<any>(
    firestore.collection("entries"),
    {
      idField: "id",
    }
  );

  if (loading) return <div>loading</div>;

  console.log({ entries });

  return (
    <List
      dataSource={entries}
      itemLayout="vertical"
      size="large"
      renderItem={(item: ListItemProps) => <ListItem {...item} />}
    />
  );
}
