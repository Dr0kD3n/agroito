import { useEffect, useContext } from "react";
import { useEvent } from "effector-react";
import { useHistory } from "react-router-dom";

import { userUnset } from "store/userStore";

import { Context } from "../..";

export const Logout = () => {
  const history = useHistory();
  const { firebase } = useContext(Context);
  const unsetUser = useEvent(userUnset);

  useEffect(() => {
    firebase
      .auth()
      .signOut()
      .then(() => {
        unsetUser();
        history.push("/");
      })
      .catch((err) => {
        console.log(err);
      });
  }, [firebase, history, unsetUser]);

  return null;
};
