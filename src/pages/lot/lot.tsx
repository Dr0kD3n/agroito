import { useContext } from "react";
import { Link, useParams } from "react-router-dom";
import { useDocumentData } from "react-firebase-hooks/firestore";
import { useEvent } from "effector-react";
import { Typography, Image, Col, Row, Space, Button } from "antd";

import { ListItemProps } from "components/list-item";
import { lotSelected } from "store/cartStore";

import { Context } from "../..";

const { Title, Text } = Typography;

export function Lot() {
  const selectLot = useEvent(lotSelected);
  const { itemId } = useParams<{ itemId: string }>();
  const { firestore } = useContext(Context);
  const [doc, isLoading] = useDocumentData<ListItemProps>(
    firestore.collection("entries").doc(itemId),
    {
      idField: "id",
    }
  );

  if (isLoading) return <div>loading</div>;

  const { author, title, description, image } = doc as ListItemProps;

  selectLot(doc as ListItemProps);

  return (
    <Row gutter={[12, 48]}>
      <Col span={24}>
        <Title level={1}>{title}</Title>
      </Col>
      <Col span={16}>
        <Row gutter={24}>
          <Col>
            <Image src={image} height={200} width={200} />
          </Col>
          <Row
            align="middle"
            justify="center"
            gutter={[0, 8]}
            style={{ alignContent: "center" }}
          >
            <Col span={24}>
              <Text>Цена: 123</Text>
            </Col>
            <Col span={24}>
              <Text>Цена: 123</Text>
            </Col>
            <Col span={24}>
              <Text>Цена: 123</Text>
            </Col>
            <Col span={24}>
              <Text>Цена: 123</Text>
            </Col>
          </Row>
        </Row>
      </Col>
      <Col span={8}>
        <Row gutter={[0, 24]} align="bottom" style={{ height: "100%" }}>
          <Col span={24}>
            <Text>Автор: {author}</Text>
          </Col>
          <Col span={24}>
            <Link to="/logistics">
              <Button>Купить</Button>
            </Link>
          </Col>
        </Row>
      </Col>
      <Col>
        <Text>{description}</Text>
      </Col>
      <Col flex="1">
        <Row justify="space-between">
          <Col>
            <Text>Сегодня в 12:30</Text>
          </Col>
          <Col>
            <Space>
              <Button type="text">Добавить в избранное</Button>
              <Button type="text">Пожаловаться</Button>
            </Space>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
