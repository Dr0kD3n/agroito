import { ReactNode, useEffect, useContext, createContext, useCallback } from "react";
import firebase from "firebase";
import { useStore, useEvent } from "effector-react";

import { $user, userSet, userUnset } from "store/userStore";

import { Context } from "../../";

export interface AuthProps {
  children: ReactNode;
}

export const AuthContext = createContext<any>(null);

const firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
  appId: process.env.REACT_APP_APP_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID,
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();

export function AuthProvider({ children }: AuthProps) {
  const { firestore } = useContext(Context);
  const currentUser = useStore($user);
  const setUser = useEvent(userSet);

  const handleAuthStateChange = useCallback(
    (user: firebase.User | null) => {
      if (!user) {
        userUnset();
        return;
      }

      firestore
        .collection("usersCollection")
        .where("uid", "==", user!.uid)
        .get()
        .then((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            const { role } = doc.data();

            setUser({
              ...user!,
              role,
            });
          });
        });
    },
    [firestore, setUser]
  );

  useEffect(() => {
    auth.onAuthStateChanged(handleAuthStateChange);
  }, [handleAuthStateChange]);

  return (
    <AuthContext.Provider value={{ currentUser }}>
      {children}
    </AuthContext.Provider>
  );
}
