import firebase from "firebase";
import {
  createStore,
  createEvent,
  createEffect,
  combine,
  sample,
  forward,
  restore,
} from "effector";

import { ListItemProps } from "components/list-item";

export interface Logist {
  id: string;
}

export interface Bank {
  id: string;
}

export const orderProcessing = createEffect<{ data: object }, void, Error>();

export const lotSelected = createEvent<ListItemProps>();

export const logistSelected = createEvent<Logist>();
export const logistDeselected = createEvent();

export const bankSelected = createEvent<Bank>();
export const bankDeselected = createEvent();

export const orderCreated = createEvent();

export const $lot = createStore<ListItemProps | null>(null);
export const $logist = createStore<Logist | null>(null);
export const $bank = createStore<Bank | null>(null);

export const $done = restore(
  orderProcessing.done.map(() => true),
  false
);

export const $error = restore(
  orderProcessing.failData.map((error) => error),
  null
);

const $order = combine({
  lot: $lot,
  logist: $logist,
  bank: $bank,
});

export const $orderStatus = combine({
  loading: orderProcessing.pending,
  done: $done,
  error: $error,
});

$lot.on(lotSelected, (_, lot) => lot);

$logist.on(logistSelected, (_, logist) => logist).reset(logistDeselected);

$bank.on(bankSelected, (_, bank) => bank).reset(bankDeselected);

orderProcessing.use(async (params) => {
  await firebase.firestore().collection("orders").add(params.data);
});

forward({
  from: sample({
    source: $order,
    clock: orderCreated,
    fn: (data) => ({ data }),
  }),
  to: orderProcessing,
});
